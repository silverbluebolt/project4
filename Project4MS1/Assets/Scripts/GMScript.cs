﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMScript : MonoBehaviour {

	public GameObject playerP;
	private GameObject playerT;
	public int lives;
	private int crntLives;
	private Vector3 spawn;



	// Use this for initialization
	void Start () {
		spawn = new Vector3(-6.0f, -3.8f);
		Instantiate (playerP, spawn, Quaternion.identity);
		playerT = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (playerT == null) {
			Instantiate (playerP, spawn, Quaternion.identity);
			playerT = GameObject.FindGameObjectWithTag("Player");
		}
	}
}
