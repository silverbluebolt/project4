﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDeath : MonoBehaviour {

    private GameObject PlayerP;
    private Vector3 respawn;

    private void Start()
    {
        PlayerP = GameObject.FindGameObjectWithTag("Player");
        respawn = new Vector3(-6.0f, -3.8f);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
    }
}
