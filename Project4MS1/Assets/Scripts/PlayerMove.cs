﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    private Transform tf;
    private Rigidbody2D rb;
    private bool canJump;
    public float speed;
    public float JForce;


    // Use this for initialization
    void Start () {
        tf = GetComponent<Transform>();
        rb = GetComponent<Rigidbody2D>();
        canJump = true;
        speed = 0.1f;
        JForce = 300.0f;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.RightArrow))
        { //when the up arrow is pressed, move forward
            tf.position += tf.right * speed;
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        { //when the down arrow is pressed, move backwards
            tf.position -= tf.right * speed;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            rb.AddForce(Vector2.up * JForce);
            canJump = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        canJump = true;
    }
}
